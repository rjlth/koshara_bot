# -*- coding: utf-8 -*-
import telebot
import constants

bot = telebot.TeleBot(constants.token)

@bot.message_handler(commands=['start'])
def handle_start(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
    user_markup.row('/start', '/help', '/stop')
    bot.send_message(message.chat.id, "Велком май диэ френд!", reply_markup=user_markup)


@bot.message_handler(commands=['help'])
def handle_text(message):
    bot.send_message(message.chat.id, """Я еще нубло, но скоро
я стану норм ботом.
А пока напиши кошара или koshara""")

@bot.message_handler(commands=['stop'])
def handle_stop(message):
    hide_markup = telebot.types.ReplyKeyboardRemove()
    bot.send_message(message.from_user.id, ':(', reply_markup=hide_markup)

@bot.message_handler(content_types=['text'])
def handle_text(message):
    if message.text.lower() == 'koshara':
        bot.send_message(message.chat.id, 'loshara')
    elif message.text.lower() == u'кошара':
        bot.send_message(message.chat.id, u'лошара')
    elif message.text.lower() == 'kex' or message.text.lower() == 'keks':
        bot.send_message(message.chat.id, 'loh')
    elif message.text.lower() == u'кекс':
        bot.send_message(message.chat.id, u'лох')
    else:
        bot.send_message(message.chat.id, 'Yurist krasava')

bot.polling(none_stop=True, interval=0)